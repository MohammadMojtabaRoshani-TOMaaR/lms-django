"""configure URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from configure import settings
from lms import views, hodView, staffView, studentView

urlpatterns = [
  path('', views.showLoginPage, name="show_login"),
  path('accounts/', include('django.contrib.auth.urls')),
  path('check_email_exist', hodView.check_email_exist, name="check_email_exist"),
  path('check_username_exist', hodView.check_username_exist, name="check_username_exist"),
  # LANDING
  path('demo/', views.showDemoPage),
  # AUTHENTICATION
  path('doLogin', views.doLogin, name="do_login"),
  path('get_user_details', views.getUserDetails),
  path('logout_user', views.logout_user,name="logout"),
  # ADMIN
  path('admin_home', hodView.admin_home, name="admin_home"),
  path('admin/', admin.site.urls),
  path('admin_profile', hodView.admin_profile, name="admin_profile"),
  path('admin_profile_save', hodView.admin_profile_save, name="admin_profile_save"),

  # STUFF
  path('add_staff', hodView.add_staff, name="add_staff"),
  path('add_stuff_save', hodView.add_stuff_save, name="add_stuff_save"),
  path('manage_stuff', hodView.manage_stuff, name="manage_stuff"),
  path('edit_staff/<str:staff_admin_id>', hodView.edit_staff, name="edit_staff"), # => url with params
  path('edit_staff_save', hodView.edit_staff_save, name="edit_staff_save"),
  path('staff_home',staffView.staff_home,name="staff_home"),
  path('staff_feedback_message', hodView.staff_feedback_message, name="staff_feedback_message"),
  path('staff_feedback_message_replied', hodView.staff_feedback_message_replied,name="staff_feedback_message_replied"),
  path('staff_leave_view', hodView.staff_leave_view, name="staff_leave_view"),
  path('staff_approve_leave/<str:leave_id>', hodView.staff_approve_leave,name="staff_approve_leave"),
  path('staff_disapprove_leave/<str:leave_id>', hodView.staff_disapprove_leave,name="staff_disapprove_leave"),

  path('staff_profile', staffView.staff_profile, name="staff_profile"),
  path('staff_profile_save', staffView.staff_profile_save, name="staff_profile_save"),
  path('staff_apply_leave', staffView.staff_apply_leave, name="staff_apply_leave"),
  path('staff_apply_leave_save', staffView.staff_apply_leave_save, name="staff_apply_leave_save"),
  path('staff_feedback', staffView.staff_feedback, name="staff_feedback"),
  path('staff_feedback_save', staffView.staff_feedback_save, name="staff_feedback_save"),
  path('staff_take_attendance', staffView.staff_take_attendance, name="staff_take_attendance"),
  path('staff_fcmtoken_save', staffView.staff_fcmtoken_save, name="staff_fcmtoken_save"),
  # COURSE
  path('add_course', hodView.add_course, name="add_course"),
  path('add_course_save', hodView.add_course_save, name="add_course_save"),
  path('manage_course', hodView.manage_course, name="manage_course"),
  path('edit_course/<str:course_id>', hodView.edit_course, name="edit_course"),  # => url with params
  path('edit_course_save', hodView.edit_course_save, name="edit_course_save"),
  # STUDENT
  path('add_student', hodView.add_student, name="add_student"),
  path('add_student_save', hodView.add_student_save, name="add_student_save"),
  path('manage_student', hodView.manage_student, name="manage_student"),
  path('edit_student/<str:student_admin_id>', hodView.edit_student, name="edit_student"),  # => url with params
  path('edit_student_save', hodView.edit_student_save, name="edit_student_save"),
  path('student_feedback_message', hodView.student_feedback_message, name="student_feedback_message"),
  path('student_feedback_message_replied', hodView.student_feedback_message_replied,name="student_feedback_message_replied"),
  path('student_leave_view', hodView.student_leave_view, name="student_leave_view"),
  path('student_approve_leave/<str:leave_id>', hodView.student_approve_leave,name="student_approve_leave"),
  path('student_disapprove_leave/<str:leave_id>', hodView.student_disapprove_leave,name="student_disapprove_leave"),

  path('student_view_attendance', studentView.student_view_attendance, name="student_view_attendance"),
  path('student_view_attendance_post', studentView.student_view_attendance_post,name="student_view_attendance_post"),

  path('get_students', staffView.get_students, name="get_students"),
  path('student_home', studentView.student_home, name="student_home"),
  path('student_fcmtoken_save', studentView.student_fcmtoken_save, name="student_fcmtoken_save"),

  path('student_profile', studentView.student_profile, name="student_profile"),
  path('student_profile_save', studentView.student_profile_save, name="student_profile_save"),
  path('student_apply_leave', studentView.student_apply_leave, name="student_apply_leave"),
  path('student_apply_leave_save', studentView.student_apply_leave_save, name="student_apply_leave_save"),
  path('student_feedback', studentView.student_feedback, name="student_feedback"),
  path('student_feedback_save', studentView.student_feedback_save, name="student_feedback_save"),
  # SUBJECT
  path('add_subject', hodView.add_subject, name="add_subject"),
  path('add_subject_save', hodView.add_subject_save, name="add_subject_save"),
  path('manage_subject', hodView.manage_subject, name="manage_subject"),
  path('edit_subject/<str:subject_id>', hodView.edit_subject, name="edit_subject"),  # => url with params
  path('edit_subject_save', hodView.edit_subject_save, name="edit_subject_save"),
  # SESSION
  path('manage_session',hodView.manage_session, name="manage_session"),
  path('add_session_save', hodView.add_session_save, name="add_session_save"),
  # ATTENDANCE
  path('staff_tack_attendance', staffView.attendance, name="staff_tack_attendance"),
  path('get_attendance_dates', staffView.get_attendance_dates, name="get_attendance_dates"),
  path('save_attendance_data', staffView.save_attendance_data, name="save_attendance_data"),
  path('staff_update_attendance', staffView.staff_update_attendance, name="staff_update_attendance"),
  path('save_attendance_data', staffView.save_attendance_data, name="save_attendance_data"),
  path('get_attendance_student', staffView.get_attendance_student, name="get_attendance_student"),
  path('save_updateattendance_data', staffView.save_updateattendance_data, name="save_updateattendance_data"),
  path('admin_view_attendance', hodView.admin_view_attendance, name="admin_view_attendance"),
  path('admin_get_attendance_dates', hodView.admin_get_attendance_dates,name="admin_get_attendance_dates"),
  path('admin_get_attendance_student', hodView.admin_get_attendance_student,name="admin_get_attendance_student"),

  path('student_view_attendance_post', studentView.student_view_attendance_post, name="student_view_attendance_post"),

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)+static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
