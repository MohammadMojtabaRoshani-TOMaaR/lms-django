from django import forms
from lms.models import Courses, SessionYearMode


class DateInput(forms.DateInput):
    input_type = "date"

class AddStudentForm(forms.Form):
    email = forms.EmailField(label='آدرس الکترونیکی', max_length= 80, widget=forms.EmailInput(attrs={"dir":"rtl", "class":"form-control", "autocomplete":"off"}))
    password = forms.CharField(label='رمز عبور', max_length=125, widget=forms.PasswordInput(attrs={"dir":"rtl", "class":"form-control"}))
    first_name = forms.CharField(label='نام', max_length=50, widget=forms.TextInput(attrs={"dir":"rtl", "class":"form-control"}))
    last_name = forms.CharField(label='نام خانوادگی', max_length=50, widget=forms.TextInput(attrs={"dir":"rtl", "class":"form-control"}))
    username = forms.CharField(label='نام کاربری', max_length=50, widget=forms.TextInput(attrs={"dir":"rtl", "class":"form-control", "autocomplete":"off"}))
    address = forms.CharField(label='آدرس', max_length=125, widget=forms.TextInput(attrs={"dir":"rtl", "class":"form-control"}))
    courses = Courses.objects.all()

    course_list = []
    courses=Courses.objects.all()
    # try:
    for course in courses:
        small_course=(course.id,course.course_name)
        course_list.append(small_course)
    # except:
    #     course_list = []

    session_list = []
    sessions = SessionYearMode.object.all()
    try:
        for session in sessions:
            small_session = (session.id, str(session.session_start_year) + "   -TO->   " + str(session.session_end_year))
            session_list.append(small_session)
    except:
        session_list = []

    gender_choice = (
        ("Male","مرد"),
        ("Female","زن")
    )

    course = forms.ChoiceField(label='درس', choices= course_list, widget=forms.Select(attrs={"dir":"rtl", "class":"form-control"}))
    sex = forms.ChoiceField(label='جنسیت', choices= gender_choice, widget=forms.Select(attrs={"dir":"rtl", "class":"form-control"}))
    session_year_id = forms.ChoiceField(label='تاریخ شروع', widget= forms.Select(attrs={"class":"form-control"}), choices=session_list)
    profile_pic = forms.FileField(label='تصویر کاربر', max_length=125 ,widget=forms.FileInput(attrs={"class":"form-control"}))

class EditStudentForm(forms.Form):
    email=forms.EmailField(label="آدرس پستی",max_length=50,widget=forms.EmailInput(attrs={"class":"form-control"}))
    first_name=forms.CharField(label="نام",max_length=50,widget=forms.TextInput(attrs={"class":"form-control"}))
    last_name=forms.CharField(label="نام خانوادگی",max_length=50,widget=forms.TextInput(attrs={"class":"form-control"}))
    username=forms.CharField(label="نام کاربری",max_length=50,widget=forms.TextInput(attrs={"class":"form-control"}))
    address=forms.CharField(label="آدرس",max_length=50,widget=forms.TextInput(attrs={"class":"form-control"}))

    course_list = []
    courses=Courses.objects.all()
    # try:
    for course in courses:
        small_course=(course.id,course.course_name)
        course_list.append(small_course)
    # except:
    #     course_list = []

    session_list = []
    sessions = SessionYearMode.object.all()
    # try:
    for session in sessions:
        small_session = (session.id, str(session.session_start_year) + "   -TO->  " + str(session.session_end_year))
        session_list.append(small_session)
    # except:
    #     session_list = []

    gender_choice=(
        ("Male","مرد"),
        ("Female","زن")
    )
    course=forms.ChoiceField(label="درس",choices=course_list,widget=forms.Select(attrs={"class":"form-control"}))
    sex=forms.ChoiceField(label="جنسیت",choices=gender_choice,widget=forms.Select(attrs={"class":"form-control"}))
    session_year_id = forms.ChoiceField(label='تاریخ شروع', widget= forms.Select(attrs={"class":"form-control"}), choices=session_list)
    profile_pic=forms.FileField(label="تصصویر کاربر",max_length=125,widget=forms.FileInput(attrs={"class":"form-control"}),required=False)