import json

from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from lms.forms import AddStudentForm, EditStudentForm
from lms.models import CustomUser, Courses, Staffs, Subjects, Students, SessionYearMode, FeedBackStaffs, \
    FeedBackStudent, LeaveReportStaff, LeaveReportStudent, Attendance, AttendanceReport


def admin_home(request):
    student_count1=Students.objects.all().count()
    staff_count=Staffs.objects.all().count()
    subject_count=Subjects.objects.all().count()
    course_count=Courses.objects.all().count()

    course_all=Courses.objects.all()
    course_name_list=[]
    subject_count_list=[]
    student_count_list_in_course=[]
    for course in course_all:
        subjects=Subjects.objects.filter(course_id=course.id).count()
        students=Students.objects.filter(course_id=course.id).count()
        course_name_list.append(course.course_name)
        subject_count_list.append(subjects)
        student_count_list_in_course.append(students)

    subjects_all=Subjects.objects.all()
    subject_list=[]
    student_count_list_in_subject=[]
    for subject in subjects_all:
        course=Courses.objects.get(id=subject.course_id.id)
        student_count=Students.objects.filter(course_id=course.id).count()
        subject_list.append(subject.subject_name)
        student_count_list_in_subject.append(student_count)

    staffs=Staffs.objects.all()
    attendance_present_list_staff=[]
    attendance_absent_list_staff=[]
    staff_name_list=[]
    for staff in staffs:
        subject_ids=Subjects.objects.filter(staff_id=staff.admin.id)
        attendance=Attendance.objects.filter(subject_id__in=subject_ids).count()
        leaves=LeaveReportStaff.objects.filter(staff_id=staff.id,leave_status=1).count()
        attendance_present_list_staff.append(attendance)
        attendance_absent_list_staff.append(leaves)
        staff_name_list.append(staff.admin.username)

    students_all=Students.objects.all()
    attendance_present_list_student=[]
    attendance_absent_list_student=[]
    student_name_list=[]
    for student in students_all:
        attendance=AttendanceReport.objects.filter(student_id=student.id,status=True).count()
        absent=AttendanceReport.objects.filter(student_id=student.id,status=False).count()
        leaves=LeaveReportStudent.objects.filter(student_id=student.id,leave_status=1).count()
        attendance_present_list_student.append(attendance)
        attendance_absent_list_student.append(leaves+absent)
        student_name_list.append(student.admin.username)


    return render(request,"hod_template/home_content.html",{"student_count":student_count1,"staff_count":staff_count,"subject_count":subject_count,"course_count":course_count,"course_name_list":course_name_list,"subject_count_list":subject_count_list,"student_count_list_in_course":student_count_list_in_course,"student_count_list_in_subject":student_count_list_in_subject,"subject_list":subject_list,"staff_name_list":staff_name_list,"attendance_present_list_staff":attendance_present_list_staff,"attendance_absent_list_staff":attendance_absent_list_staff,"student_name_list":student_name_list,"attendance_present_list_student":attendance_present_list_student,"attendance_absent_list_student":attendance_absent_list_student})

def add_staff(request):
    return render(request,"hod_template/add_staff_template.html")

# stuffs methods

def add_stuff_save(request):
    if request.method != 'POST':
        return  HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        first_name= request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        username = request.POST.get('username')
        address = request.POST.get('address')
        email = request.POST.get('email')
        password = request.POST.get('password')

        try:
            user = CustomUser.objects.create_user(
                first_name=first_name,
                last_name=last_name,
                username=username,
                email=email,
                password=password,
                user_type=2
            )
            user.staffs.address = address
            user.save()
            messages.success(request, "کامند با موفقیت به ثبت رسید :)")
            return HttpResponseRedirect(reverse("add_staff"))
        except :
            messages.error(request, "مشکل در ثبت کاربر :(")
            return HttpResponseRedirect(reverse("add_staff"))

def manage_stuff(request):
    stuffs = Staffs.objects.all()
    return render(request, "hod_template/manage_stuff_template.html",{
        'stuffs': stuffs
    })

def edit_staff(request, staff_admin_id):
    staff = Staffs.objects.get(admin = staff_admin_id)
    return render(request, 'hod_template/edit_staff_template.html',{
        'staff': staff
    })

def edit_staff_save(request):
    if request.method != 'POST':
        return HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        staff_id = request.POST.get('staff_id')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        username = request.POST.get('username')
        address = request.POST.get('address')

        try:
            # fetch and modify staff in customUser table
            user = CustomUser.objects.get(id = staff_id)
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.username = username
            user.save()
            # fetch and modify staff in staff table
            staff_model = Staffs.objects.get(admin = staff_id)
            staff_model.address = address
            staff_model.save()
            messages.success(request, "کارمند با موفقیت تصحیح شد :]")
            return HttpResponseRedirect(reverse("edit_staff",kwargs={"staff_admin_id":staff_id}))
        except:
            messages.error(request, "خطا در تصحیح کارمند :[")
            return HttpResponseRedirect(reverse("edit_staff",kwargs={"staff_admin_id":staff_id}))

# COURSESs methods

def add_course(request):
    return render(request, 'hod_template/add_course_template.html')


def add_course_save(request):
    if request.method != 'POST':
        return HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        name=request.POST.get('course')
        try:
            course_model= Courses(course_name= name)
            course_model.save()
            messages.success(request, "درس با موفقیت به ثبت رسید :)")
            return HttpResponseRedirect(reverse ("add_course"))
        except:
            messages.error(request, "مشکل در ثبت درس :(")
            return HttpResponseRedirect(reverse ("add_course"))

def manage_course(request):
    courses = Courses.objects.all()
    return render(request, 'hod_template/manage_course_template.html',{
        'courses': courses
    })

def edit_course(request, course_id):
    courses = Courses.objects.get(id=course_id)
    return render(request, 'hod_template/edit_course_template.html', {
        'course': courses
    })
    pass

def edit_course_save(request):
    if request.method != 'POST':
        return HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        course_id = request.POST.get('course_id')
        course_name = request.POST.get('course')

        try:
            # fetch and modify course from course table by id
            course = Courses.objects.get(id= course_id)
            course.course_name = course_name
            course.save()
            messages.success(request, "درس با موفقیت به تصحیح شد :]")
            return HttpResponseRedirect(reverse("edit_course",kwargs={"course_id":course_id}))
        except NameError:
            messages.error(request, "خطا در تصحیح درس :[" + NameError)
            return HttpResponseRedirect(reverse("edit_course",kwargs={"course_id":course_id}))


# STUDENTs methods

def add_student(requset):
    courses = Courses.objects.all()
    form = AddStudentForm()
    return  render(requset,'hod_template/add_student_template.html', {
        'form': form
    })

def add_student_save(request):
    if request.method != 'POST':
        return  HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        form = AddStudentForm(request.POST,request.FILES)
        if form.is_valid():
            first_name= form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            username = form.cleaned_data['username']
            address = form.cleaned_data['address']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            session_year_id = form.cleaned_data['session_year_id']
            course_id  = form.cleaned_data['course']
            sex  = form.cleaned_data['sex']
            # ready file and upload
            profile_pic = request.FILES['profile_pic']
            fileStorage = FileSystemStorage()
            file_name = fileStorage.save(profile_pic.name,profile_pic)
            profile_pic_url = fileStorage.url(file_name)
            try:
                user = CustomUser.objects.create_user(
                    first_name=first_name,
                    last_name=last_name,
                    username=username,
                    email=email,
                    password=password,
                    user_type=3
                )
                user.students.address = address
                courses_object = Courses.objects.get(id=course_id)
                user.students.course_id= courses_object
                user.students.gender = sex
                session = SessionYearMode.object.get(id= session_year_id)
                user.students.session_year_id = session
                user.students.profile_pic = profile_pic_url
                user.save()
                messages.success(request, "دانش جو با موفقیت به ثبت رسید :)")
                return HttpResponseRedirect(reverse ("add_student"))
            except NameError:
                messages.error(request, "مشکل در ثبت دانش جو :(" + NameError)
                return HttpResponseRedirect(reverse ("add_student"))
        else:
            form = AddStudentForm(request.POST)
            return render(request,'hod_template/add_student_template.html', {
            'form': form
        })

def manage_student(request):
    students = Students.objects.all()
    return render(request, 'hod_template/manage_student_template.html',{
        'students': students
    })

def edit_student(request, student_admin_id):
    request.session['student_admin_id'] = student_admin_id
    student = Students.objects.get(admin=student_admin_id)
    form = EditStudentForm()
    form.fields['email'].initial = student.admin.email
    form.fields['first_name'].initial = student.admin.first_name
    form.fields['last_name'].initial = student.admin.last_name
    form.fields['username'].initial = student.admin.username
    form.fields['address'].initial = student.address
    form.fields['course'].initial = student.course_id.id
    form.fields['sex'].initial = student.gender
    form.fields['session_year_id'].initial = student.session_year_id.id
    return render(request, 'hod_template/edit_student_template.html',{
        'form': form,
        "username": student.admin.username
    })

def edit_student_save(request):
    if request.method != 'POST':
        return  HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        student_id = request.session.get('student_admin_id')
        if student_id == None:
            return HttpResponseRedirect(reverse ("manage_student"))

        form= EditStudentForm(request.POST, request.FILES)
        if form.is_valid():
            first_name= form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            username = form.cleaned_data['username']
            address = form.cleaned_data['address']
            email = form.cleaned_data['email']
            session_year_id = form.cleaned_data['session_year_id']
            course_id  = form.cleaned_data['course']
            sex  = form.cleaned_data['sex']
            # ready file and upload
            if request.FILES.get('profile_pic', False):
                profile_pic = request.FILES['profile_pic']
                fileStorage = FileSystemStorage()
                file_name = fileStorage.save(profile_pic.name,profile_pic)
                profile_pic_url = fileStorage.url(file_name)
            else:
                profile_pic_url = None
            try:
                # fetch and modify student from customUser table
                user = CustomUser.objects.get(id= student_id)
                user.first_name = first_name
                user.last_name = last_name
                user.username = username
                user.email = email
                # fetch and modify student from student table
                student = Students.objects.get(admin=student_id)
                student.address = address
                student.gender = sex

                session = SessionYearMode.object.get(id= session_year_id)
                student.session_year_id = session

                if profile_pic_url != None:
                    student.profile_pic = profile_pic_url
                # fetch course assigned to student from course table by it's id
                course = Courses.objects.get(id= course_id)
                student.course_id = course
                # save changes
                user.save()
                student.save()

                del request.session['student_admin_id']
                messages.success(request, "دانش جو با موفقیت به تصحیح شد :]")
                return HttpResponseRedirect(reverse("edit_student",kwargs={"student_admin_id":student_id}))
            except NameError:
                messages.error(request,"خطا در تصحیح دانشجو :[" + NameError)
                return HttpResponseRedirect(reverse("edit_student",kwargs={"student_admin_id":student_id}))
        else:
            form=EditStudentForm(request.POST)
            student = Students.objects.get(admin=student_id)
            return render(request, 'hod_template/edit_subject_template.html',{
                'form': form,
                "username": student.admin.username
            })
# SUBJECT methods
def add_subject(request):
    courses = Courses.objects.all()
    staffs = CustomUser.objects.filter(user_type=2)
    return  render(request,'hod_template/add_subject_template.html', {
        'courses': courses,
        'staffs': staffs
    })

def add_subject_save(request):
    if request.method != 'POST':
        return  HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        subject_name = request.POST.get('subject')
        course_id = request.POST.get('course')
        staff_id = request.POST.get('staff')

        try:
            staff = CustomUser.objects.get(id = staff_id)
            course = Courses.objects.get(id = course_id)
            subject = Subjects(
                subject_name = subject_name,
                course_id= course,
                staff_id= staff
            ).save()
            messages.success(request, "موضوع با موفقیت به ثبت رسید :)")
            return HttpResponseRedirect(reverse("add_subject"))
        except NameError:
            messages.error(request, "مشکل در ثبت موضوع :(" + NameError)
            return HttpResponseRedirect(reverse("add_subject"))

def manage_subject(request):
    subjects = Subjects.objects.all()
    print(subjects)
    return render(request, 'hod_template/manage_subject_template.html',{
        'subjects': subjects
    })

def edit_subject(request, subject_id):
    subjects = Subjects.objects.get(id = subject_id)
    courses = Courses.objects.all()
    staffs = CustomUser.objects.filter(user_type=2)
    return render(request, 'hod_template/edit_subject_template.html',{
        'subject': subjects,
        'courses': courses,
        'staffs': staffs
    })

def edit_subject_save(request):
    if request.method != 'POST':
        return HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        subject_id = request.POST.get('subject_id')
        subject_name = request.POST.get('subject')
        staff_id = request.POST.get('staff')
        course_id = request.POST.get('course')

        try:
            # fetch and modify subject from subject table by id
            subject = Subjects.objects.get(id=subject_id)
            subject.subject_name = subject_name
            # fetch  staff&course
            staff = CustomUser.objects.get(id= staff_id)
            course = Courses.objects.get(id = course_id)

            subject.staff_id = staff
            subject.course_id = course

            subject.save()

            messages.success(request, "موضوع با موفقیت به تصحیح شد :]")
            return HttpResponseRedirect(reverse("edit_subject",kwargs={"subject_id":subject_id}))
        except NameError:
            messages.error(request, "خطا در تصحیح موضوع :[" + NameError)
            return HttpResponseRedirect(reverse("edit_subject",kwargs={"subject_id":subject_id}))

# SESSION
def manage_session(request):
    return render(request, 'hod_template/manage_session_template.html')

def add_session_save(request):
    if request.method != 'POST':
        return HttpResponse('ارسال فرم با متد غیر مجاز')
    else:
        sesstion_start_year = request.POST.get('session_start')
        sesstion_end_year = request.POST.get('session_end')

        try:
            SessionYearMode(
                session_start_year=sesstion_start_year,
                session_end_year=sesstion_end_year
            ).save()
            messages.success(request, "دوره با موفقیت به اضافه شد :)")
            return HttpResponseRedirect(reverse("manage_session"))
        except NameError:
            messages.error(request, "خطا در اضافه کردن دوره :(" + NameError)
            return HttpResponseRedirect(reverse("manage_session"))

# EMAIL$USERNAME checker

@csrf_exempt
def check_email_exist(request):
    email=request.POST.get("email")
    user_obj=CustomUser.objects.filter(email=email).exists()
    if user_obj:
        return HttpResponse(True)
    else:
        return HttpResponse(False)

@csrf_exempt
def check_username_exist(request):
    username=request.POST.get("username")
    user_obj=CustomUser.objects.filter(username=username).exists()
    if user_obj:
        return HttpResponse(True)
    else:
        return HttpResponse(False)

# FEEDBACK

def staff_feedback_message(request):
    feedbacks=FeedBackStaffs.objects.all()
    return render(request,"hod_template/staff_feedback_template.html",{"feedbacks":feedbacks})

def student_feedback_message(request):
    feedbacks=FeedBackStudent.objects.all()
    return render(request,"hod_template/student_feedback_template.html",{"feedbacks":feedbacks})

@csrf_exempt
def student_feedback_message_replied(request):
    feedback_id=request.POST.get("id")
    feedback_message=request.POST.get("message")

    try:
        feedback=FeedBackStudent.objects.get(id=feedback_id)
        feedback.feedback_reply=feedback_message
        feedback.save()
        return HttpResponse("True")
    except:
        return HttpResponse("False")

@csrf_exempt
def staff_feedback_message_replied(request):
    feedback_id=request.POST.get("id")
    feedback_message=request.POST.get("message")

    try:
        feedback=FeedBackStaffs.objects.get(id=feedback_id)
        feedback.feedback_reply=feedback_message
        feedback.save()
        return HttpResponse("True")
    except:
        return HttpResponse("False")

# LEAVE

def staff_leave_view(request):
    leaves=LeaveReportStaff.objects.all()
    return render(request,"hod_template/staff_leave_view.html",{"leaves":leaves})

def student_leave_view(request):
    leaves=LeaveReportStudent.objects.all()
    return render(request,"hod_template/student_leave_view.html",{"leaves":leaves})

def student_approve_leave(request,leave_id):
    leave=LeaveReportStudent.objects.get(id=leave_id)
    leave.leave_status=1
    leave.save()
    return HttpResponseRedirect(reverse("student_leave_view"))

def student_disapprove_leave(request,leave_id):
    leave=LeaveReportStudent.objects.get(id=leave_id)
    leave.leave_status=2
    leave.save()
    return HttpResponseRedirect(reverse("student_leave_view"))


def staff_approve_leave(request,leave_id):
    leave=LeaveReportStaff.objects.get(id=leave_id)
    leave.leave_status=1
    leave.save()
    return HttpResponseRedirect(reverse("staff_leave_view"))

def staff_disapprove_leave(request,leave_id):
    leave=LeaveReportStaff.objects.get(id=leave_id)
    leave.leave_status=2
    leave.save()
    return HttpResponseRedirect(reverse("staff_leave_view"))


# ATTENDANCE

def admin_view_attendance(request):
    subjects=Subjects.objects.all()
    session_year_id=SessionYearMode.object.all()
    return render(request,"hod_template/admin_view_attendance.html",{"subjects":subjects,"session_year_id":session_year_id})

@csrf_exempt
def admin_get_attendance_dates(request):
    subject=request.POST.get("subject")
    session_year_id=request.POST.get("session_year_id")
    subject_obj=Subjects.objects.get(id=subject)
    session_year_obj=SessionYearMode.object.get(id=session_year_id)
    attendance=Attendance.objects.filter(subject_id=subject_obj,session_year_id=session_year_obj)
    attendance_obj=[]
    for attendance_single in attendance:
        data={"id":attendance_single.id,"attendance_date":str(attendance_single.attendance_date),"session_year_id":attendance_single.session_year_id.id}
        attendance_obj.append(data)

    return JsonResponse(json.dumps(attendance_obj),safe=False)


@csrf_exempt
def admin_get_attendance_student(request):
    attendance_date=request.POST.get("attendance_date")
    attendance=Attendance.objects.get(id=attendance_date)

    attendance_data=AttendanceReport.objects.filter(attendance_id=attendance)
    list_data=[]

    for student in attendance_data:
        data_small={"id":student.student_id.admin.id,"name":student.student_id.admin.first_name+" "+student.student_id.admin.last_name,"status":student.status}
        list_data.append(data_small)
    return JsonResponse(json.dumps(list_data),content_type="application/json",safe=False)

# ADMIN

def admin_profile(request):
    user=CustomUser.objects.get(id=request.user.id)
    return render(request,"hod_template/admin_profile.html",{"user":user})

def admin_profile_save(request):
    if request.method!="POST":
        return HttpResponseRedirect(reverse("admin_profile"))
    else:
        first_name=request.POST.get("first_name")
        last_name=request.POST.get("last_name")
        password=request.POST.get("password")
        try:
            customuser=CustomUser.objects.get(id=request.user.id)
            customuser.first_name=first_name
            customuser.last_name=last_name
            # if password!=None and password!="":
            #     customuser.set_password(password)
            customuser.save()
            messages.success(request, "اطلاعات کاربری با موفقیت اصلاح شد :)")
            return HttpResponseRedirect(reverse("admin_profile"))
        except:
            messages.error(request, "خطا در ثبت اطلاعات کاربری :(")
            return HttpResponseRedirect(reverse("admin_profile"))