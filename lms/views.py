from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from lms.EmailBackEnd import EmailBackEnd
# Create your views here.
def showDemoPage(request):
    return render(request,'demo.html')

def showLoginPage(request):
    return render(request,"login_page.html")

def doLogin(request):
    if request.method != 'POST':
        return HttpResponse("<h3>ارسال فرم با متد غیر مجاز<h3>")
    else:
        user=EmailBackEnd.authenticate(
            request,
            username=request.POST.get('email'),
            password=request.POST.get("password")
        )
        if user != None:
            login(request,user)



            if user.user_type == "1":
                return HttpResponseRedirect("/admin_home") # pass => ADMINs
            elif user.user_type == "2":
                return HttpResponseRedirect(reverse("staff_home")) # pass => STAFFs
            elif user.user_type == "3":
                return HttpResponseRedirect(reverse("student_home")) # pass => STUDENTs
            else:
                return HttpResponse("THERE IS NO SPECIFIED ROLE IN SYSTEM | " + "نقشی برای شما در سیستم ما موجود نیست")
        else:
            messages.error(request,"Invalid Login Details")
            return  HttpResponseRedirect("/")

def getUserDetails(request):
    if request.user != None:
        return HttpResponse("Email: " + request.user.email + " UserType: " + request.user.user_type)
    else:
        return HttpResponse("ابتدا باید لاگین کنید")

def logout_user(request):
    logout(request)
    return HttpResponseRedirect("/")

