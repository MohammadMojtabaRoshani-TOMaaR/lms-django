from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponse, HttpResponseRedirect


class LoginCheckMiddelware(MiddlewareMixin):
    def process_view(
            self,
            request,
            view_func,
            view_args,
            view_kwargs):
            moduleName = view_func.__module__  # find which file the request has been sent
            user = request.user

            if user.is_authenticated:
                # admin access control
                if user.user_type == "1":
                    if moduleName == "lms.hodView":
                        pass
                    elif moduleName == "lms.views" or moduleName == "django.contrib.admin.sites" or moduleName == "django.contrib.admin.options" or moduleName == "django.views.static":
                        pass
                    else:
                        return HttpResponseRedirect(reverse("admin_home"))

                # staff access control
                elif user.user_type == "2":
                    if moduleName == "lms.staffView":
                        pass
                    elif moduleName == "lms.views" or moduleName == "django.views.static":
                        pass
                    else:
                        return HttpResponseRedirect(reverse("staff_home"))
                # student access control
                elif user.user_type == "3":
                    if moduleName == "lms.studentView":
                        pass
                    elif moduleName == "lms.views" or moduleName == "django.views.static":
                        pass
                    else:
                        return HttpResponseRedirect(reverse("student_home"))
                else:
                    return HttpResponseRedirect(reverse("show_login"))

            else:
                if request.path == reverse("show_login") or request.path == reverse("do_login") or moduleName == "django.contrib.auth.views" :
                    pass
                else:
                    return HttpResponseRedirect(reverse("show_login"))
