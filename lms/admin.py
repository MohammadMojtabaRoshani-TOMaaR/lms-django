from django.contrib import admin

# Register your models here.
# make blank user model for password encryption
from django.contrib.auth.admin import UserAdmin

from lms.models import CustomUser, Courses


class UserModel(UserAdmin):
    pass

# linking two user models
admin.site.register(CustomUser, UserModel)

# testing
admin.site.register(Courses)
